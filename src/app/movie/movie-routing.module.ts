import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailMovieComponent } from './containers/detail-movie/detail-movie.component';
import { ListMovieComponent } from './containers/list-movie/list-movie.component';


const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'detail/:id', component: DetailMovieComponent },
  { path: 'list', component: ListMovieComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovieRoutingModule { }
