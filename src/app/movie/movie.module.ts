import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MovieRoutingModule } from './movie-routing.module';
import { ListMovieComponent } from './containers/list-movie/list-movie.component';
import { DetailMovieComponent } from './containers/detail-movie/detail-movie.component';

@NgModule({
  declarations: [ListMovieComponent, DetailMovieComponent],
  imports: [
    CommonModule,
    MovieRoutingModule
  ]
})
export class MovieModule { }
