import { Component, OnInit } from '@angular/core';
import { MovieService } from '../../services/movie.service';
import { Movie } from 'src/app/shared/models/movie';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-detail-movie',
  templateUrl: './detail-movie.component.html',
  styleUrls: ['./detail-movie.component.scss']
})
export class DetailMovieComponent implements OnInit {
  movie: Movie;
  movies: Movie[];



  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap((params) => {
        const id = params.get('id');
        return this.movieService.get(id);
      })
    )
      .subscribe(movie => this.movie = movie);

    this.movieService.search("Iron Man").subscribe(movies => {
      this.movies = movies;
    });
  }

}
