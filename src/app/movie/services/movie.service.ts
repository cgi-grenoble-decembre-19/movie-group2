import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Movie } from 'src/app/shared/models/movie';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  resource = 'movie';
  constructor(private http: HttpClient) { }

  get(id: string) {
    return this.http.get<Movie>(`${environment.urlApi}/${this.resource}/${id}${environment.paramApi}`).pipe(
      map(movie => new Movie(movie)),
      tap(movie => console.log(movie))
    );
  }

  search(query: string) {
    /*return this.http.get<Movie[]>(`${environment.urlApi}/${this.resource}/${id}${environment.paramApi}`).pipe(
      map(movie => new Movie(movie)),
      tap(movie => console.log(movie))
    );*/

    return this.http.get(`${environment.urlApi}/search/${this.resource}${
      environment.paramApi}&query=${encodeURIComponent(query)}`).pipe(
        tap(data => console.log(data)),
        map((data: any) => data.results),
        map((movies: Movie[]) => movies.map(movie => new Movie(movie))),
        tap(data => console.log(data))
      );
  }
}
