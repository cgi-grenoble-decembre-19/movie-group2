import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './containers/layout/layout.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { MovieModule } from '../movie/movie.module';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [LayoutComponent, HeaderComponent, FooterComponent],
  imports: [
    CommonModule,
    RouterModule,
    MovieModule,
    SharedModule
  ],
  exports: [
    LayoutComponent
  ]
})
export class UiModule { }
