import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

// Lazy loading du module avec définition du chemin au niveau du routeur
const routes: Routes = [
  {
    path: 'movie', loadChildren: () => import('./movie/movie.module').then(m => m.MovieModule)
  }
  // {
  //   path: 'clients',
  //   loadChildren: () => import('./clients/clients.module').then(m => m.ClientsModule)
  // },
];

@NgModule({
  // crée l'instance de router pour le projet + ajout du preload
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
