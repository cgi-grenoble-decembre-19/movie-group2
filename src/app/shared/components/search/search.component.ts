import { Component, ViewChild } from '@angular/core';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject, merge, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, switchMap } from 'rxjs/operators';
import { MovieService } from 'src/app/movie/services/movie.service';
import { Router } from '@angular/router';

const defFilms = [];

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  model: any;

  constructor(private movieService: MovieService,
    private router: Router) { }

  @ViewChild('instance', { static: true }) instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  done(term: string) {
    if (term !== '') {
      this.movieService.search(term).subscribe(
        films => {
          console.log(films);
          console.log(`${films[0].id}`);
          films.length > 0 ? this.router.navigate(['/movie', 'detail', `${films[0].id}`]) : null;
        });
    }
  }

  search = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      switchMap(term => (term === '' ? of(defFilms)
        : this.movieService.search(term).pipe(
          map((value => value.map(t => t.title).slice(1, 10)))
        )
      ))
    );
  }
}
