export enum MovieStatus {
 RUMORED = 'Rumored',
 PLANNED = 'Planned',
 IN_PRODUCTION = 'In Production',
 POST_PRODUCTION = 'Post Production',
 RELEASED = 'Released',
 CANCELED = 'Canceled',
}
