export class Genre {
  id: number;
  name: string;

  constructor(fields?: Partial<Genre>) {
    if (fields) {
      Object.assign(this, fields);
    }
  }
}
