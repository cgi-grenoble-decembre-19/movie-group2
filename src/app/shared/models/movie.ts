import { MovieStatus } from '../enums/movie-status.enum';
import { Genre } from './genre';

export class Movie {

  adult: boolean;
  budget: number;
  genres: Genre[];
  id: number;
  poster_path: string;
  overview: string;
  release_date: string;
  runtime: number;
  status: MovieStatus;
  title: string;
  vote_average: number;
  vote_count: number;

  constructor(fields?: Partial<Movie>) {
    if (fields) {
      Object.assign(this, fields);
    }
  }
}
